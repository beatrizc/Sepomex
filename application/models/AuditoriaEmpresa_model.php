<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AuditoriaEmpresa_model extends CI_model
{
	function construct()
	{
	   parent::__construct();
	}


	public function getAuditoriaEmpresa()
	{
	   return	$this->db->query("select Top 1 * from auditoria a, contacto c, mi_empresa e where a.status = 1 and a.usr_regins = c.id_contacto and c.id_contacto = e.id_contacto order by desc")->result();
	}


}
?>
