<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NuevaEmpresa_Controller extends CI_Controller {

public function index()
	{
		$this->load->view("Empresa/create");
	}

public function SaveEmpresa()
	{
	  if ($this->input->post())
	  {
	  	$this->load->model("Empresa_model");
	  	 $nombre_mi_empresa = $this->db->escape($_POST[$nombre_mi_empresa]);
	   $rfc_mi_empresa = $this->db->escape($_POST[$rfc_mi_empresa]);
	   if (! $_POST[$id_contacto]) 
	   	  $id_c = $_POST[$id_contacto]; //si el contacto existe, le asignamos el código
	   	else
	   	  $id_c = 0; //	0 si no es un usuario de sistema
	  	if ($this->Empresa_model->SaveEmpresa($id_c, $nombre_mi_empresa, $rfc_mi_empresa))
	  	{
	  		header("Location:".base_url()."NuevaEmpresa_controller/SaveEmpresa");
	  	}
	  }
	}

public function ModifyEmpresa($id_mi_empresa)
{
	$id_mi_empresa = $this->db->escape($id_mi_empresa);
	$empresa = $this->Empresa_model->get1Empresa($id_mi_empresa); //buscamos la empresa por id_mi_empresa
    $this->layout->view("/Empresa/edit", compact("empresa"); //enviamos el id de la empresa
}

}